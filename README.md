# Pete FineUploader Bundle for Symfony2

## Installation

### Add bundle to your composer.json file

``` js
// composer.json

{
    "require": {
		// ...
        "pete/fineuploader-bundle": "dev-master"
    }
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:RighteousIdealizedDung/petefineuploaderbundle.git"
        }
    ]
}
```

### Add bundle to your application kernel

``` php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Pete\FineUploaderBundle\petefineuploaderBundle(),
        // ...
    );
}
```

### Download the bundle using Composer

``` bash
$ php composer.phar update pete/fineuploader-bundle
```

## Licenses

Refer to the source code of the included files for license information

## References

1. http://docs.fineuploader.com/
2. http://symfony.com
